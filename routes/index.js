const express = require('express');

const router = express.Router();
const auth = require('../controllers/authController');

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', { title: 'Express' });
});

router.post('/register', auth.register);
router.post('/login', auth.login);

module.exports = router;

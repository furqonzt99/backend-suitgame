const express = require('express');
const { body } = require('express-validator');

const router = express.Router();
const { fight } = require('../controllers/fightController');
const tokenAuthentication = require('../middlewares/jwtMiddleware');

router.post(
  '/',
  body('suit').not().isEmpty().withMessage('suit is required'),
  tokenAuthentication,
  fight,
);

module.exports = router;

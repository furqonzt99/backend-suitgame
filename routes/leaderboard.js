const express = require('express');

const router = express.Router();
const { leaderboard, leaderboardExport } = require('../controllers/leaderboardController');

router.get('/', leaderboard);
router.get('/export', leaderboardExport);

module.exports = router;

const express = require('express');

const router = express.Router();
const users = require('../controllers/usersController');
const tokenAuthentication = require('../middlewares/jwtMiddleware');

router.get('/profile', tokenAuthentication, users.profile);
router.put('/update', tokenAuthentication, users.update);
router.put('/photo', tokenAuthentication, users.photo);
router.delete('/delete', tokenAuthentication, users.delete);

module.exports = router;

const bcrypt = require('bcrypt');

module.exports = {
  async up(queryInterface) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    const password = '1234qwer';
    const data = [];
    const passwordHash = bcrypt.hashSync(password, 10);

    for (let index = 1; index <= 25; index + 1) {
      const userData = {
        name: `User ${index}`,
        email: `user${index}@gmail.com`,
        password: passwordHash,
        bornDate: new Date('1999-06-15'),
        point: Math.floor(Math.random() * 100),
        createdAt: new Date(),
        updatedAt: new Date(),
      };
      data.push(userData);
    }

    await queryInterface.bulkInsert('users', data);
  },

  async down(queryInterface) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkInsert('users', null, {});
  },
};

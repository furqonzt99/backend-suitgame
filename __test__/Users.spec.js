const request = require('supertest');
const app = require('../app');

describe('Get Profile', () => {
    const profile = () => request(app)
        .get('/users/profile');

    it('success 200 get profile', async () => {
        const response = await profile();
        expect(response.status).toBe(200);
    });
});
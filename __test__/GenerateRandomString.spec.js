const { generateRandomString } = require('../helpers/generateRandomString');

describe('Generate Random String', () => {
    it('success generate 5 char', () => {
        const rs = generateRandomString(5);
        expect(rs.length).toBe(5);
    })

    it('success generate 10 char', () => {
        const rs = generateRandomString(10);
        expect(rs.length).toBe(10);
    })
    
    it('success generate 20 char', () => {
        const rs = generateRandomString(20);
        expect(rs.length).toBe(20);
    })
})
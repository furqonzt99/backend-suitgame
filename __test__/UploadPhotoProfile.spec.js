const request = require('supertest');
const app = require('../app');
const { generateRandomString } = require('../helpers/generateRandomString')

const filePhotoPath = `${__dirname}/assets/photo.jpeg`;
const filePdfPath = `${__dirname}/assets/test.pdf`;

let username; let email; let name; let password; let bornDate; let
  token;

// init var
username = generateRandomString(8);
email = `${username}@gmail.com`;
name = `nama ${username}`;
password = username;
bornDate = '1999-06-15';

describe('Register user', () => {
  const validRegUser = {
    username,
    password,
    name,
    email,
    bornDate,
  };

  const register = () => request(app)
    .post('/register')
    .send(validRegUser);

  it('Register return 200 when success', async () => {
    const response = await register();
    expect(response.status).toBe(200);
  });
});

describe('Login user', () => {
  const validLoginUser = {
    username,
    password,
  };

  const login = () => request(app)
    .post('/login')
    .send(validLoginUser);

  it('Login return 200 when success', async () => {
    const response = await login();
    expect(response.status).toBe(200);
    token = response._body.data;
  });
});

describe('Upload Profile', () => {
  const uploadPhotoValid = () => request(app)
    .put('/photo')
    .set('Authorization', `Bearer ${token}`)
    .attach('photo', filePhotoPath);

  const uploadPhotoInvalid401 = () => request(app).put('/photo').send({});

  const uploadPhotoInvalid500 = () => request(app)
    .put('/photo')
    .set('Authorization', `Bearer ${token}`)
    .attach('photo', filePdfPath);

  it('success 200 upload photo profile', async () => {
    const response = await uploadPhotoValid();
    expect(response.status).toBe(200);
  });

  it('failed 401 unauthorized upload photo profile', async () => {
    const response = await uploadPhotoInvalid401();
    expect(response.status).toBe(401);
  });

  it('failed 500 wrong file type upload photo profile', async () => {
    const response = await uploadPhotoInvalid500();
    expect(response.status).toBe(500);
  });
});

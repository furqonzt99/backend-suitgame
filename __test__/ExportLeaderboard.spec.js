const request = require('supertest');
const app = require('../app');

describe('Export Leaderboard', () => {
  const exportLeaderboard = () => request(app)
    .get('/leaderboard/export');

  it('success 200 export leaderboard', async () => {
    const response = await exportLeaderboard();
    expect(response.status).toBe(200);
  });
});
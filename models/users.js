require('dotenv').config();
const { Model } = require('sequelize');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const cloudinary = require('cloudinary').v2;

module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      users.hasMany(models.Fight, {
        foreignKey: 'user_id',
      });
    }

    static uploadPhoto = async (id, photo) => {
      try {
        const mime = photo.mimetype
        const mimeParse = mime.split("/")
        const validMime = mimeParse[0]
        if (validMime !== 'image') {
          return Promise.reject('file type must be an image!')
        }

        const upload = await cloudinary.uploader
          .upload(photo.tempFilePath,
            {
              resource_type: 'image',
              transformation: { width: 150, crop: "pad" }
            }
          );

        const query = {
          where: { id: id },
        };

        return this.update(
          {
            photo: upload.secure_url
          },
          query,
        );
      } catch(err) {
        return Promise.reject(err);
      }
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = async ({
      name, email, username, password, bornDate,
    }) => {
      const encryptedPassword = this.#encrypt(password);
      try {
        const users = await this.findOne({ where: { username } });
        if (users) return Promise.reject('Username already used!');
        const emails = await this.findOne({ where: { email } });
        if (emails) return Promise.reject('Email already used!');
        return this.create({
          name,
          email,
          username,
          password: encryptedPassword,
          bornDate,
        });
      } catch (err) {
        return Promise.reject(err);
      }
    };

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    generateToken = () => {
      const payload = {
        id: this.id,
        username: this.username,
      };

      const secretKey = process.env.SECRET_KEY_JWT;

      const token = jwt.sign(payload, secretKey, { expiresIn: '2d' });

      return token;
    };

    static authenticate = async ({ username, password }) => {
      try {
        const users = await this.findOne({ where: { username } });
        if (!users) return Promise.reject('User not found!');
        const isPasswordValid = users.checkPassword(password);
        if (!isPasswordValid) return Promise.reject('Wrong Password');
        return Promise.resolve(users);
      } catch (err) {
        return Promise.reject(err);
      }
    };
  }
  users.init(
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      photo: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      email: {
        type: DataTypes.STRING,
        validate: {
          notEmpty: {
            args: true,
            msg: 'Email-id required',
          },
          isEmail: {
            args: true,
            msg: 'Valid email-id required',
          },
        },
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      bornDate: {
        type: DataTypes.DATE,
        get() {
          const rawValue = this.getDataValue('bornDate');
          return rawValue ? rawValue.toISOString().substring(0, 10) : null;
        },
      },
      point: {
        type: DataTypes.BIGINT,
        defaultValue: 0,
      },
    },
    {
      sequelize,
      modelName: 'users',
    },
  );
  return users;
};

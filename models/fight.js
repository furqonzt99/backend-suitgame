const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class Fight extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.users, {
        foreignKey: 'user_id',
      });
    }
  }
  Fight.init(
    {
      user_id: DataTypes.BIGINT,
      you: DataTypes.STRING,
      opponent: DataTypes.STRING,
      result: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: 'Fight',
    },
  );
  return Fight;
};

const { users } = require('../models');

const usergame = {
  async profile(req, res) {
    try {
      const user = await users.findOne({
        where: {
          id: req.authenticatedUser.id,
        },
        attributes: {
          exclude: ['password', 'updatedAt', 'createdAt'],
        },
      });

      return res.status(200).json({
        code: 200,
        message: 'Successfull Operation',
        data: user,
      });
    } catch (error) {
      return res.status(500).json({
        code: 500,
        message: error,
      });
    }
  },

  async update(req, res) {
    try {
      const user = req.body;
      const query = {
        where: { id: req.authenticatedUser.id },
      };
      await users.update(
        {
          name: user.name,
          email: user.email,
          username: user.username,
          bornDate: user.bornDate,
        },
        query,
      );
      return res.status(200).json({
        code: 200,
        message: 'Successful Operation',
      });
    } catch (error) {
      return res.status(500).json({
        code: 500,
        message: error,
      });
    }
  },

  async photo(req, res) {
    const { photo } = req.files;
    try {
      await users.uploadPhoto(req.authenticatedUser.id, photo);
      return res.status(200).json({
        code: 200,
        message: 'Successful Operation',
      });
    } catch (error) {
      return res.status(500).json({
        code: 500,
        message: error,
      });
    }
  },

  async delete(req, res) {
    try {
      await users
        .destroy({
          where: { id: req.authenticatedUser.id },
        })
        .then(() => res.status(200).json({
          code: 200,
          message: 'Successful Operation',
        }));
    } catch (error) {
      return res.status(500).json({
        code: 500,
        message: error,
      });
    }
    return null;
  },
};

module.exports = usergame;

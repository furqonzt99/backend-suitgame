const fs = require('fs');
const PDFDocument = require('pdfkit-table');
const { users } = require('../models');

function formatLeaderboard(data, key) {
  return {
    rank: parseInt(key + 1, 10),
    name: data.name,
    point: data.point,
  };
}

function formatExportLeaderboard(data, key) {
  return [parseInt(key + 1, 10), data.name, data.point];
}

const leaderboard = async (req, res) => {
  try {
    const data = await users.findAll({
      order: [['point', 'DESC']],
      limit: 10,
    });

    const dataFormat = [];

    data.forEach((value, index) => {
      dataFormat.push(formatLeaderboard(value, index));
    });

    return res.status(200).json({
      code: 200,
      message: 'Successful Operation',
      data: dataFormat,
    });
  } catch (err) {
    return res.status(500).json({
      code: 500,
      message: 'Internal server error',
    });
  }
};

const leaderboardExport = async (req, res) => {
  try {
    const data = await users.findAll({
      order: [['point', 'DESC']],
      limit: 10,
    });

    const dataFormat = [];

    data.forEach((value, index) => {
      dataFormat.push(formatExportLeaderboard(value, index));
    });

    // start pdf document
    const doc = new PDFDocument({ margin: 30, size: 'A4' });
    // to save on server
    doc.pipe(fs.createWriteStream('./leaderboard.pdf'));

    const tableArray = {
      title: 'Suitgame',
      subtitle: 'Top 10 Leaderboard',
      headers: ['Rank', 'Name', 'Point'],
      rows: dataFormat,
    };
    doc.table(tableArray, { width: 500 }); // A4 595.28 x 841.89 (portrait) (about width sizes)

    doc.pipe(res);

    // done
    doc.end();

    return null;
  } catch (err) {
    return res.status(500).json({
      code: 500,
      message: err,
    });
  }
};

module.exports = {
  leaderboard,
  leaderboardExport,
};

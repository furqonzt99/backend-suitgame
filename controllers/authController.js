const { users } = require('../models');

function format(UserGames) {
  return {
    code: 200,
    message: 'Successful Operation',
    data: UserGames.generateToken(),
  };
}

module.exports = {
  async register(req, res) {
    const {
      name, email, username, password, bornDate,
    } = req.body;
    await users
      .register({
        name,
        email,
        username,
        password,
        bornDate,
      })
      .then(() => res.status(200).json({
        status: 200,
        message: 'Successful Operation',
      }))
      .catch((error) => res.status(400).json({
        status: 400,
        message: error,
      }));
  },

  login: (req, res) => {
    users
      .authenticate(req.body)
      .then((user) => {
        const data = format(user);
        return res.status(200).json(data);
      })
      .catch((error) => res.status(400).json({
        status: 400,
        message: error,
      }));
  },
};

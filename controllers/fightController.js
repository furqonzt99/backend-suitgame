const { validationResult } = require('express-validator');
const { Fight, users } = require('../models');
const db = require('../models/index');

const randomChoose = () => {
  const option = ['R', 'P', 'S'];
  return option[Math.floor(Math.random() * 3)];
};

const gameResult = (you, com) => {
  if (you === com) {
    return 'DRAW';
  }

  if (you === 'R' && com === 'S') {
    return 'WIN';
  }

  if (you === 'P' && com === 'R') {
    return 'WIN';
  }

  if (you === 'S' && com === 'P') {
    return 'WIN';
  }

  return 'LOSE';
};

const gamePoint = (result) => {
  if (result === 'DRAW') {
    return 0;
  }

  if (result === 'WIN') {
    return 20;
  }
  return -10;
};

const fight = async (req, res) => {
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }

  try {
    const user = await users.findOne({
      where: {
        id: req.authenticatedUser.id,
      },
      attributes: {
        exclude: ['password'],
      },
    });

    const comSuit = randomChoose();
    const result = gameResult(req.body.suit, comSuit);

    const data = {
      user_id: req.authenticatedUser.id,
      you: req.body.suit,
      opponent: comSuit,
      result,
    };

    const point = user.point + gamePoint(result);

    await db.sequelize.transaction(async (t) => {
      await Fight.create(data, { transaction: t });

      await users.update(
        {
          point,
        },
        { where: { id: req.authenticatedUser.id } },
        { transaction: t },
      );

      return user;
    });

    return res.status(200).json({
      code: 200,
      message: 'Successful Operation',
      data,
    });
  } catch (err) {
    return res.status(500).json({ code: 500, message: err });
  }
};

module.exports = {
  fight,
};

require('dotenv').config();

const {
  DB_USER, DB_PASSWORD, DB_NAME, DB_HOST, DB_DIALECT,
  CLOUDINARY_NAME, CLOUDINARY_KEY, CLOUDINARY_SECRET,
} = process.env;

module.exports = {
  development: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    dialect: DB_DIALECT,
    cloudinary_name: CLOUDINARY_NAME,
    cloudinary_key: CLOUDINARY_KEY,
    cloudinary_secret: CLOUDINARY_SECRET,
  },
  test: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    dialect: DB_DIALECT,
    cloudinary_name: CLOUDINARY_NAME,
    cloudinary_key: CLOUDINARY_KEY,
    cloudinary_secret: CLOUDINARY_SECRET,
  },
  production: {
    username: DB_USER,
    password: DB_PASSWORD,
    database: DB_NAME,
    host: DB_HOST,
    dialect: DB_DIALECT,
    cloudinary_name: CLOUDINARY_NAME,
    cloudinary_key: CLOUDINARY_KEY,
    cloudinary_secret: CLOUDINARY_SECRET,
    dialectOptions: {
      ssl: {
        rejectUnauthorized: false,
      },
    },
  },
};

const jwt = require('jsonwebtoken');

async function tokenAuthentication(req, res, next) {
  const tokenBearer = req.headers.authorization;
  let token;

  if (tokenBearer) {
    const bearer = tokenBearer.split(' ');
    const [, t] = bearer;
    token = t;
  } else {
    return res.status(401).json({ code: 401, message: 'invalid token' });
  }

  const secretKey = process.env.SECRET_KEY_JWT;

  if (token) {
    try {
      const decoded = jwt.verify(token, secretKey);

      req.authenticatedUser = decoded;
    } catch (err) {
      return res.status(401).json({ code: 401, message: 'invalid token' });
    }
  }
  next();
  return null;
}

module.exports = tokenAuthentication;
